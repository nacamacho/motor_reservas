<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExtraTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('extra', function(Blueprint $table)
		{
		    $table->char('id',36);
		    $table->string('name_en');
		    $table->string('name_es');
		    $table->string('name_de');
		    $table->string('name_fr');
		    $table->string('name_pt');
		    $table->longText('description_en');
		    $table->longText('description_es');
		    $table->longText('description_de');
		    $table->longText('description_fr');
		    $table->longText('description_pt');
		    $table->double('price_a',15,5);
		    $table->double('price_b',15,5);
		    $table->double('price_c',15,5);

		    // 1 => Por Habitacion 	| 2 => Por Dias | 3 => Por Reserva
		    $table->integer('type');

		    // 1 => Un dia | 2 => Varios Dias | 3 => Todos los dias
		    $table->string('applies');


		    //FIELDS STATICS

		    $table->timestamps();
		    $table->char('created_by_id',36);
		    $table->char('updated_by_id',36);
		    $table->integer('item_state');

		    //KEYS 

		    $table->primary('id');
		    $table->foreign('created_by_id')->references('id')->on('user');
			$table->foreign('updated_by_id')->references('id')->on('user');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExists('extra');
	}

}
