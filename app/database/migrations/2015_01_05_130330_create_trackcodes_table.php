<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackcodesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('trackcode', function(Blueprint $table)
		{
		    $table->char('id',36);
		    $table->string('code');
		    $table->string('name');
		    $table->longText('description');
		    $table->char('website',50);

		    //FIELDS STATICS

		    $table->timestamps();
		    $table->char('created_by_id',36);
		    $table->char('updated_by_id',36);
		    $table->integer('item_state');

		    //KEYS 

		    $table->primary('id');
			$table->foreign('created_by_id')->references('id')->on('user');
			$table->foreign('updated_by_id')->references('id')->on('user');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExists('trackcode');
	}

}
