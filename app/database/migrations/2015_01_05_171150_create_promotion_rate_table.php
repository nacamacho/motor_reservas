<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePromotionRateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('promotion_rate', function(Blueprint $table)
		{
		    $table->char('id',36);
		    $table->char('promotion_id',36);
		    $table->char('rate_id',36);

		    //FIELDS STATICS

		    $table->timestamps();
		    $table->char('created_by_id',36);
		    $table->char('updated_by_id',36);
		    $table->integer('item_state');

		    //KEYS 

		    $table->primary('id');
		    $table->foreign('promotion_id')->references('id')->on('promotion');
		    $table->foreign('rate_id')->references('id')->on('rate');
		    $table->foreign('created_by_id')->references('id')->on('user');
			$table->foreign('updated_by_id')->references('id')->on('user');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExists('promotion_rate');
	}

}
