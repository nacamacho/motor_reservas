<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('campaign', function(Blueprint $table)
		{
		    $table->char('id',36);
		    $table->string('name');
		    $table->double('price_a',15,5);
		    $table->double('price_b',15,5);
		    $table->double('price_c',15,5);
		    $table->double('price',15,5);
		    $table->integer('amount_promocodes');
		    $table->integer('discount_rate');
		   	$table->dateTime('start_date');
		   	$table->dateTime('end_date');

		    //FIELDS STATICS

		    $table->timestamps();
		    $table->char('created_by_id',36);
		    $table->char('updated_by_id',36);
		    $table->integer('item_state');

		    //KEYS 

		    $table->primary('id');
			$table->foreign('created_by_id')->references('id')->on('user');
			$table->foreign('updated_by_id')->references('id')->on('user');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExists('campaign');
	}

}
