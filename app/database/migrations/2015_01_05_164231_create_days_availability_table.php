<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDaysAvailabilityTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('availability_days', function(Blueprint $table)
		{
		    $table->char('id',36);
		    $table->date('day');
		    $table->char('room_id',36);
		    
		    // Promocion => PR | Plan => PL | Tarifa => TR | Disponibilidad => DP
		    $table->string('type');
		    $table->string('name');
		    $table->string('value');


		    //FIELDS STATICS

		    $table->timestamps();
		    $table->char('created_by_id',36);
		    $table->char('updated_by_id',36);
		    $table->integer('item_state');

		    //KEYS 

		    $table->primary('id');
		    $table->foreign('room_id')->references('id')->on('room');
		    $table->foreign('created_by_id')->references('id')->on('user');
			$table->foreign('updated_by_id')->references('id')->on('user');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExists('availability_days');
	}

}
