<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('country', function(Blueprint $table)
		{
		    $table->char('id',36);
		    $table->string('name_en');
		    $table->string('name_es');
		    $table->string('name_de');
		    $table->string('name_fr');
		    $table->string('name_pt');
		    $table->string('iso_currency');
		    $table->string('iso_2');
		    $table->string('iso_3');
		    $table->integer('number_code');
		    $table->double('exchange',15,5);
		    $table->char('continent_id',36);

		    //FIELDS STATICS

		    $table->timestamps();
		    $table->char('created_by_id',36);
		    $table->char('updated_by_id',36);
		    $table->integer('item_state');

		    //KEYS 

		    $table->primary('id');
		    $table->foreign('continent_id')->references('id')->on('continent');
		    $table->foreign('created_by_id')->references('id')->on('user');
			$table->foreign('updated_by_id')->references('id')->on('user');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExists('country');
	}

}
