<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomFeaturesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('room_features', function(Blueprint $table)
		{
		    $table->char('id',36);
		    $table->char('room_id',36);
		    $table->char('feature_id',36);

		    //FIELDS STATICS

		    $table->timestamps();
		    $table->char('created_by_id',36);
		    $table->char('updated_by_id',36);
		    $table->integer('item_state');

		    //KEYS 

		    $table->primary('id');
		    $table->foreign('room_id')->references('id')->on('room');
		    $table->foreign('feature_id')->references('id')->on('feature');
		    $table->foreign('created_by_id')->references('id')->on('user');
			$table->foreign('updated_by_id')->references('id')->on('user');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExists('room_features');
	}

}
