<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHotelTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('hotel', function(Blueprint $table)
		{
		    $table->char('id',36);
		    $table->string('name');
		    $table->string('address');
		    $table->string('latitude');
		    $table->string('longitude');
		    $table->string('facebook');		
		    $table->string('twitter');		
		    $table->string('instagram');		
		    $table->string('youtube');		
		    $table->string('four');		
		    $table->string('email_1');		
		    $table->string('email_2');		
		    $table->string('email_3');		
		    $table->string('phone_1');		
		    $table->string('phone_2');		
		    $table->string('phone_3');	
		    $table->string('maximum_days');	
		    $table->string('category');	
		    $table->string('stars');	

		    $table->char('city_id',36);
		    $table->char('country_id',36);

		    //FIELDS STATICS

		    $table->timestamps();
		    $table->char('created_by_id',36);
		    $table->char('updated_by_id',36);
		    $table->integer('item_state');

		    //KEYS 

		    $table->primary('id');
		    $table->foreign('city_id')->references('id')->on('city');
		    $table->foreign('country_id')->references('id')->on('country');
		    $table->foreign('created_by_id')->references('id')->on('user');
			$table->foreign('updated_by_id')->references('id')->on('user');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
		Schema::dropIfExists('hotel');
	}

}
